/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manager for the cache of the MDR.
 * 
 * @author diogo
 *
 */
public final class CacheManager {

    /**
     * Maximum size of the cache.
     */
    private static final int CACHE_MAXIMUM_SIZE = 10000;
    
    /**
     * The cache instance.
     */
    private static LoadingCache<CacheKey, String> jsonCache;

    /**
     * The logger for this class.
     */
    private static Logger logger = LoggerFactory.getLogger(CacheManager.class.getName());

    /**
     * Static factory method to obtain a cache instance.
     * 
     * @param mdrClient
     *            MDR Client instance, to be reused (with path and proxy settings already loaded).
     * @return cache instance
     */
    public static LoadingCache<CacheKey, String> getCache(final MdrClient mdrClient) {
        if (jsonCache == null) {
            CacheLoader<CacheKey, String> loader = new CacheLoader<CacheKey, String>() {
                public String load(final CacheKey key) throws MdrConnectionException {
                    logger.debug("Element was not in cache: " + key.getPath() + " | " + key.getLanguageCode() + " | "
                            + key.getAccessToken());
                    if (key.getAccessToken() != null) {
                        return mdrClient.getJsonFromMdr(key.getPath(), key.getLanguageCode(), key.getAccessToken());
                    } else {
                        return mdrClient.getJsonFromMdr(key.getPath(), key.getLanguageCode());
                    }
                }
            };
            jsonCache = CacheBuilder.newBuilder().maximumSize(CACHE_MAXIMUM_SIZE).build(loader);
        }

        return jsonCache;
    }

    /**
     * Cleans the cache of a specific MDR Client.
     * 
     * @param mdrClient
     *            the MDR Client for which the cache will be cleaned.
     */
    public static void cleanCache(final MdrClient mdrClient) {
        getCache(mdrClient).invalidateAll();
        logger.debug("Cache cleaned.");
    }

    /**
     * Prevent instantiation.
     */
    private CacheManager() {
    }
}
