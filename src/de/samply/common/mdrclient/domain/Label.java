/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

/**
 * Label of a data element.
 * 
 * @author diogo
 *
 */
public class Label {

    /**
     * Language of the label.
     */
    private String language;
    /**
     * Designation of the label.
     */
    private String designation;
    /**
     * Definition of the label.
     */
    private String definition;

    /**
     * Get the language of this label.
     * @return The language of the label
     */
    public final String getLanguage() {
        return language;
    }

    /**
     * Set the language of this label.
     * @param language
     *            The language of the label
     */
    public final void setLanguage(final String language) {
        this.language = language;
    }

    /**
     * Get the designation of this label.
     * @return The designation of the label
     */
    public final String getDesignation() {
        return designation;
    }

    /**
     * Set the designation of this label.
     * @param designation
     *            The designation of the label
     */
    public final void setDesignation(final String designation) {
        this.designation = designation;
    }

    /**
     * Get the definition of this label.
     * @return The definition of the label
     */
    public final String getDefinition() {
        return definition;
    }

    /**
     * Set the definition of this label.
     * @param definition
     *            The definition of the label
     */
    public final void setDefinition(final String definition) {
        this.definition = definition;
    }

}