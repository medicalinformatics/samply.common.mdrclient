/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Key-value pairs as defined in the MDR.
 * 
 * @author diogo
 *
 */
public class Slot {

    /**
     * The key.
     */
    @SerializedName("slot_name")
    private String slotName;
    /**
     * The value.
     */
    @SerializedName("slot_value")
    private String slotValue;

    /**
     * Get the slot name (i.e. key).
     * @return The slot name
     */
    public final String getSlotName() {
        return slotName;
    }

    /**
     * Set the slot name (i.e. key).
     * @param slotName
     *            The slot name
     */
    public final void setSlotName(final String slotName) {
        this.slotName = slotName;
    }

    /**
     * Get the slot value.
     * @return The slot value
     */
    public final String getSlotValue() {
        return slotValue;
    }

    /**
     * Set the slot value.
     * @param slotValue
     *            The slot value
     */
    public final void setSlotValue(final String slotValue) {
        this.slotValue = slotValue;
    }

}