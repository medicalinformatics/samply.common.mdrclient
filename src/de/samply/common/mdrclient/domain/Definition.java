/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import java.util.ArrayList;

/**
 * Designation/definition and the value domain of an MDR data element.
 * 
 * @author diogo
 * 
 */
public class Definition {
    
    /**
     * Data element designations.
     */
    private ArrayList<Record> designations;
    
    /**
     * Data element validations.
     */
    private Validations validation;

    /**
     * Definition of a data element.
     */
    public Definition() {
    }

    /**
     * Get the designations of this data element.
     * @return the designations of this data element.
     */
    public final ArrayList<Record> getDesignations() {
        return designations;
    }

    /**
     * Set the designations of this data element.
     * @param designations
     *            the new designations.
     */
    public final void setDesignations(final ArrayList<Record> designations) {
        this.designations = designations;
    }

    /**
     * Get the validations of this data element.
     * @return the validations of this data element.
     */
    public final Validations getValidation() {
        return validation;
    }

    /**
     * Set the validations of this data element.
     * @param validation
     *            validation of this data element.
     */
    public final void setValidation(final Validations validation) {
        this.validation = validation;
    }

}
