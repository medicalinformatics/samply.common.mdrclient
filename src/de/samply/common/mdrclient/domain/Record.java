/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

/**
 * A group of data elements with a common designation.
 * 
 * @author diogo
 *
 */
public class Record {
    /**
     * The definition of the record.
     */
    private String definition;
    /**
     * The designation of the record.
     */
    private String designation;
    /**
     * The language of the record.
     */
    private String language;

    /**
     * 
     * @return the definition of the record
     */
    public final String getDefinition() {
        return this.definition;
    }

    /**
     * 
     * @param definition
     *            the definition of the record
     */
    public final void setDefinition(final String definition) {
        this.definition = definition;
    }

    /**
     * 
     * @return the designation of the record
     */
    public final String getDesignation() {
        return this.designation;
    }

    /**
     * 
     * @param designation
     *            the designation of the record
     */
    public final void setDesignation(final String designation) {
        this.designation = designation;
    }

    /**
     * 
     * @return the language of the record
     */
    public final String getLanguage() {
        return this.language;
    }

    /**
     * 
     * @param language
     *            the language of the record
     */
    public final void setLanguage(final String language) {
        this.language = language;
    }
}
