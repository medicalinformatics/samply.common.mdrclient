/**
 * Copyright (C) 2016 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * A namespace response from the MDR.
 * 
 * @author michael
 *
 */
public class Namespace {
    
    /** The name of the namespace. */
    private String name;
    
    /** Is the namespace writeable?. */
    private boolean writeable;
    
    /**
     * Designations of the namespace.
     */
    private List<Designation> designations = new ArrayList<Designation>();
    
    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Checks if the namespace is writeable.
     *
     * @return true, if is writeable
     */
    public boolean isWriteable() {
        return writeable;
    }

    /**
     * Sets the writeable.
     *
     * @param writeable the new writeable
     */
    public void setWriteable(boolean writeable) {
        this.writeable = writeable;
    }

    /**
     * Gets the designations.
     *
     * @return the designations
     */
    public List<Designation> getDesignations() {
        return designations;
    }

    /**
     * Sets the designations.
     *
     * @param designations the new designations
     */
    public void setDesignations(List<Designation> designations) {
        this.designations = designations;
    }

}
