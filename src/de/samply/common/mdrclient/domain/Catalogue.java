/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * A catalogue.
 * 
 * @author diogo
 *
 */
public class Catalogue {

    /**
     * Codes from the catalogue.
     */
    private List<Code> codes = new ArrayList<Code>();

    /**
     * The root element and its subcodes.
     */
    private Root root;

    /**
     * 
     * @return The codes
     */
    public final List<Code> getCodes() {
        return codes;
    }

    /**
     * 
     * @param codes
     *            The codes
     */
    public final void setCodes(final List<Code> codes) {
        this.codes = codes;
    }

    /**
     * 
     * @return The root
     */
    public final Root getRoot() {
        return root;
    }

    /**
     * 
     * @param root
     *            The root
     */
    public final void setRoot(final Root root) {
        this.root = root;
    }

}