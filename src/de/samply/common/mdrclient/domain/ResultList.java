/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * A result list of results.
 * 
 * @see Result
 * 
 * @author diogo
 *
 */
public class ResultList {
    
    /**
     * List of results.
     * 
     * @see Result
     */
    private List<Result> results = new ArrayList<Result>();
    
    /**
     * The number of results, given by the MDR.
     */
    private Integer totalcount;

    /**
     * Get the result list.
     * @return the result list
     */
    public final List<Result> getResults() {
        return results;
    }

    /**
     * Set the result list.
     * @param results
     *            the list of results
     */
    public final void setResults(final List<Result> results) {
        this.results = results;
    }

    /**
     * Get the number of results.
     * @return the number of results
     */
    public final Integer getTotalcount() {
        return totalcount;
    }

    /**
     * Set the number of results.
     * @param totalcount
     *            the number of results
     */
    public final void setTotalcount(final Integer totalcount) {
        this.totalcount = totalcount;
    }
}
