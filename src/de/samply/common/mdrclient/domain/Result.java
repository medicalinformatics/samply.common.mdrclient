/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mdrclient.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * A result response from the MDR.
 * 
 * @author diogo
 *
 */
public class Result {
    /**
     * The id of the result.
     */
    private String id;

    /**
     * The name of the data type.
     */
    private String type;

    /**
     * The cardinal of the result.
     */
    private int order;

    /**
     * The Identification element, containing the urn and the status of the dataelement
     */
    private Identification identification;

    /**
     * Designations of the result.
     */
    private List<Designation> designations = new ArrayList<Designation>();

    /**
     * Get the id of this result.
     * 
     * @return The id
     */
    public final String getId() {
        return id;
    }

    /**
     * Set the id of this result.
     * 
     * @param id
     *            The id
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
     * Get the type of this result.
     * 
     * @see EnumElementType
     * @return The type of this result
     */
    public final String getType() {
        return type;
    }

    /**
     * Set the type of this result.
     * 
     * @param type
     *            The new type
     */
    public final void setType(final String type) {
        this.type = type;
    }

    /**
     * Get the order of this result.
     * 
     * @return The order of this result
     */
    public final int getOrder() {
        return order;
    }

    /**
     * Set the order of this result.
     * 
     * @param order
     *            The new order
     */
    public final void setOrder(final int order) {
        this.order = order;
    }

    /**
     * Get the identification of this result.
     *
     * @return The identification of this result
     */
    public Identification getIdentification() {
        return identification;
    }

    /**
     * Set the identification of this result.
     *
     * @param identification
     *            The new identification
     */
    public void setIdentification(Identification identification) {
        this.identification = identification;
    }

    /**
     * Get the designations of this result.
     * 
     * @return The designations of this result
     */
    public final List<Designation> getDesignations() {
        return designations;
    }

    /**
     * Set the designations of this result.
     * 
     * @param designations
     *            The new designations
     */
    public final void setDesignations(final List<Designation> designations) {
        this.designations = designations;
    }

}